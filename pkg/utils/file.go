package utils

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
)

func OpenFileForRead(filename string) (*os.File, error) {
	name := filepath.Clean(filename)

	file, err := os.OpenFile(name, os.O_RDONLY, 0666)
	if errors.Is(err, fs.ErrPermission) {
		return nil, fmt.Errorf("'%s': read %w", filename, err)
	}
	if err != nil {
		return nil, fmt.Errorf("'%s': %w", filename, err)
	}

	return file, nil
}
