package utils

import (
	"os"
)

type (
	userError struct {
		Success bool   `json:"success"`
		Error   string `json:"error"`
	}
)

func PrintUserError(err error) {
	PrettyPrintJson(userError{
		Error: err.Error(),
	})
}

func PrintFatalUserError(err error) {
	PrintUserError(err)
	os.Exit(1)
}
