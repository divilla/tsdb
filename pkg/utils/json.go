package utils

import (
	"encoding/json"
	"fmt"
)

func PrettyPrintJson(i interface{}) {
	jb, _ := json.MarshalIndent(i, "", "  ")
	fmt.Println(string(jb))
}
