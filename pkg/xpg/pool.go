package xpg

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Pool struct {
	*pgxpool.Pool
}

func NewPool(connString string) (*Pool, error) {
	cfg, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, fmt.Errorf("invalid database connection string: %w\n", err)
	}

	pool, err := pgxpool.NewWithConfig(context.Background(), cfg)
	if err != nil {
		return nil, fmt.Errorf("database connection failed: %w\n", err)
	}

	return &Pool{
		pool,
	}, nil
}

func (p *Pool) SetMinConns(n uint16) {
	p.Config().MinConns = int32(n)
	p.Config().MaxConns = p.Config().MinConns * 8
}
