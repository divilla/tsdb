#!/bin/bash -e

#exec > >(tee -a /var/log/app/entry.log|logger -t cpusage -s 2>/dev/console) 2>&1

echo ""
echo "[`date`] Importing data..."
./cpusage import data/cpu_usage.csv

echo ""
echo "[`date`] Running queries with 64 workers..."
./cpusage exec -workers 64 data/query_params.csv
