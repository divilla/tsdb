package main

import (
	"gitlab.com/divilla/tsdb/internal/application"
	"gitlab.com/divilla/tsdb/internal/cli_app"
	"gitlab.com/divilla/tsdb/internal/component"
	"gitlab.com/divilla/tsdb/internal/config"
	"gitlab.com/divilla/tsdb/internal/repository"
	"gitlab.com/divilla/tsdb/pkg/utils"
	"gitlab.com/divilla/tsdb/pkg/xpg"
	"go.uber.org/zap"
	"os"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	defer func() { _ = logger.Sync() }()

	cfg, err := config.NewConfig()
	if err != nil {
		utils.PrintFatalUserError(err)
	}

	pool, err := xpg.NewPool(cfg.PgConnStr())
	if err != nil {
		utils.PrintFatalUserError(err)
	}
	defer pool.Close()

	cur := repository.NewCpuUsage(logger, pool)
	qpr := repository.NewQueryParams(logger, cfg)
	rep := component.NewReporter(cfg)
	app := application.New(logger, cfg, pool, cur, qpr, rep)

	if err = cli_app.NewApp(app).Run(os.Args); err != nil {
		utils.PrintFatalUserError(err)
	}
}
