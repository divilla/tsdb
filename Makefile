MODULE = $(shell go list -m)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || echo "1.0.0")
PACKAGES := $(shell go list ./cmd/... ./internal/... ./pkg/...)
LDFLAGS := -ldflags "-X main.Version=${VERSION}"

TIMESTAMP=$(shell date +'%Y-%m-%d_%H:%M:%S')
DB_CONTAINER = cpusage-timescaledb

PID_FILE := './.pid'
FSWATCH_FILE := './fswatch.cfg'

.PHONY: default
default: help

# generate help info from comments: thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## help information about make commands
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## run unit tests
	@echo "mode: count" > coverage-all.out
	@$(foreach pkg,$(PACKAGES), \
		go test -p=1 -cover -covermode=count -coverprofile=coverage.out ${pkg}; \
		tail -n +2 coverage.out >> coverage-all.out;)

.PHONY: test-cover
test-cover: test ## run unit tests and show test coverage information
	go tool cover -html=coverage-all.out

.PHONY: clean
clean: ## remove temporary files
	rm -rf server coverage.out coverage-all.out

.PHONY: run
run: ## run the API server
	go run ${LDFLAGS} cmd/console/main.go

.PHONY: build
build:  ## build the API server binary
	CGO_ENABLED=0 go build ${LDFLAGS} -a -o cpusage $(MODULE)/cmd/console/.

.PHONY: build-docker
build-docker: ## build the API server as a docker image
	docker build -f cmd/docker/Dockerfile -t cpusage-app .

.PHONY: db-up
db-up: ## start the database server
	docker run \
		--name ${DB_CONTAINER} \
		-p 5432:5432 \
		-v "$(shell pwd)/docker/timescaledb/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d" \
		-v "$(shell pwd)/docker/timescaledb/timescaledb:/bitnami/postgresql" \
		-e POSTGRESQL_PASSWORD=pass \
		-e POSTGRESQL_DATABASE=cpusage \
		-e POSTGRESQL_TIMEZONE=GMT \
		-e POSTGRESQL_LOG_TIMEZONE=GMT \
		-e POSTGRESQL_SHARED_PRELOAD_LIBRARIES=timescaledb \
		-d docker.io/timescale/timescaledb:2.8.1-pg14-bitnami

.PHONY: db-down
db-down: ## stop the database server
	@docker stop ${DB_CONTAINER}
	@docker container rm ${DB_CONTAINER}

.PHONY: db-ssh
db-ssh: ## stop the database server
	docker exec -it ${DB_CONTAINER} /bin/bash

.PHONY: lint
lint: ## run golint on all Go package
	golint $(PACKAGES)

.PHONY: fmt
fmt: ## run "go fmt" on all Go packages
	@go fmt $(PACKAGES)

.PHONY: version
version: ## display the version of the API server
	@echo $(VERSION)
