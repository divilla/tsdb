#!/bin/bash
################################
# run before docker-compose up #
################################

mkdir -p docker/timescaledb/docker-entrypoint-initdb.d
chgrp -R docker docker/timescaledb/docker-entrypoint-initdb.d
chmod -R ug+rwx docker/timescaledb/docker-entrypoint-initdb.d
chown 1001 docker/timescaledb/docker-entrypoint-initdb.d

rm -rf docker/timescaledb/timescaledb
mkdir -p docker/timescaledb/timescaledb
chgrp docker docker/timescaledb/timescaledb
chmod ug+rwx docker/timescaledb/timescaledb
chown 1001 docker/timescaledb/timescaledb
