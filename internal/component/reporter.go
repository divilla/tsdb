package component

import (
	"gitlab.com/divilla/tsdb/internal/config"
	"gitlab.com/divilla/tsdb/internal/dto"
	"gitlab.com/divilla/tsdb/internal/entity"
	"sort"
	"time"
)

type (
	// Reporter is decoupled from QueryParamsRepository for the following reasons:
	// 1. It can potentially be reused in other parts of the system
	// 2. Single-Responsibility Principle
	// 3. Easier to write unit testing
	// 4. Better readability
	Reporter struct {
		config      *config.Config
		queryParams []*entity.QueryParams
	}
)

func NewReporter(cfg *config.Config) *Reporter {
	return &Reporter{
		config: cfg,
	}
}

func (r *Reporter) Add(qp *entity.QueryParams) {
	r.queryParams = append(r.queryParams, qp)
}

func (r *Reporter) Report() dto.ReportResponse {
	var qps, qpsErr []*entity.QueryParams
	var total time.Duration
	var res dto.ReportResponse

	for _, qp := range r.queryParams {
		if !qp.Success {
			qpsErr = append(qpsErr, qp)
			continue
		}

		qps = append(qps, qp)
		total += qp.Duration
	}

	if len(qps) == 0 {
		return res
	}

	if len(qps) == 1 {
		val := r.toPrecisionString(qps[0].Duration)

		return dto.ReportResponse{
			Count:       1,
			TotalTime:   val,
			MinTime:     val,
			MaxTime:     val,
			MedianTime:  val,
			AverageTime: val,
		}
	}

	sort.Slice(qps, func(i, j int) bool {
		return int64(qps[i].Duration) < int64(qps[j].Duration)
	})

	count := len(qps)
	min := qps[0].Duration
	max := qps[count-1].Duration
	avg := total / time.Duration(count)

	half := count / 2
	var med time.Duration
	if count%2 == 1 {
		med = qps[half].Duration
	} else {
		upper := qps[half].Duration
		lower := qps[half-1].Duration
		med = (upper + lower) / 2
	}

	return dto.ReportResponse{
		Success:     len(qpsErr) == 0,
		Count:       count,
		TotalTime:   r.toPrecisionString(total),
		MinTime:     r.toPrecisionString(min),
		MaxTime:     r.toPrecisionString(max),
		MedianTime:  r.toPrecisionString(med),
		AverageTime: r.toPrecisionString(avg),
		ExecErrors:  qpsErr,
	}
}

func (r *Reporter) toPrecisionString(d time.Duration) string {
	return d.Round(r.config.TimePrecision()).String()
}
