package cli_app

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/divilla/tsdb/internal/application"
	"gitlab.com/divilla/tsdb/pkg/utils"
	"os"
	"os/signal"
	"sync"
	"time"
)

func NewApp(app *application.Application) *cli.App {
	return &cli.App{
		Name:     "cpusage",
		Usage:    "benchmark SELECT query performance",
		Version:  "v0.1.0",
		Compiled: time.Now(),
		Authors: []*cli.Author{
			{
				Name:  "Vito Di Villa",
				Email: "vitodivilla@gmail.com",
			},
		},
		Copyright: "(c) 2022 Di Villa's Enterprise",

		Commands: []*cli.Command{
			{
				Name:            "import",
				Aliases:         []string{"i"},
				Usage:           "cpusage import 'path/to/file.csv'",
				Description:     "imports data from specified csv file",
				SkipFlagParsing: false,
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() == 0 {
						return fmt.Errorf("'path/to/file.csv' argument is required")
					}

					filename := ctx.Args().First()
					er, err := app.Import(filename)
					if err != nil {
						return err
					}

					utils.PrettyPrintJson(er)

					return nil
				},
			},
			{
				Name:    "exec",
				Aliases: []string{"e"},
				Usage:   "cpusage exec --workers 16 'path/to/file.csv'",
				//UsageText: "reads query params from csv file, executes queries in specified number of workers & reports stats",
				Description:     "reads query params from csv file, concurrently executes queries in specified number of workers & reports stats",
				SkipFlagParsing: false,
				Flags: []cli.Flag{
					&cli.UintFlag{Name: "workers", Aliases: []string{"w"}, Required: true},
					&cli.BoolFlag{Name: "debug", Aliases: []string{"d"}, Required: false},
				},
				Action: func(ctx *cli.Context) error {
					if ctx.Args().Len() == 0 {
						return fmt.Errorf("'path/to/file.csv' argument is required")
					}

					workersCount := ctx.Uint("workers")
					if err := app.SetWorkersCount(workersCount); err != nil {
						return err
					}

					dm := ctx.Bool("debug")
					if dm {
						app.SetDebugMode()
					}

					quitCh := make(chan os.Signal, 1)
					signal.Notify(quitCh, os.Interrupt)
					stopCh := make(chan struct{})
					wg := &sync.WaitGroup{}

					filename := ctx.Args().First()
					importErrors, err := app.ExecuteQueries(filename, quitCh, stopCh, wg)
					if err != nil {
						return err
					}

					sig := <-quitCh
					close(stopCh)
					wg.Wait()

					rr := app.Report(sig, importErrors)
					utils.PrettyPrintJson(rr)

					return nil
				},
			},
		},
	}
}
