package application

import (
	"context"
	"fmt"
	"gitlab.com/divilla/tsdb/internal/component"
	"gitlab.com/divilla/tsdb/internal/config"
	"gitlab.com/divilla/tsdb/internal/dto"
	"gitlab.com/divilla/tsdb/internal/entity"
	"gitlab.com/divilla/tsdb/pkg/utils"
	"gitlab.com/divilla/tsdb/pkg/xpg"
	"go.uber.org/zap"
	"io"
	"os"
	"runtime"
	"sync"
	"syscall"
)

type (
	CpuUsageRepository interface {
		Import(ctx context.Context, r io.Reader) (er *dto.ExecResponse, err error)
		ExecuteQuery(ctx context.Context, qp *entity.QueryParams) (er *dto.ExecResponse, err error)
	}

	QueryParamsRepository interface {
		MakeBuckets(i uint)
		Import(r io.Reader) ([]string, error)
		Get(bucketKey int) *entity.QueryParams
		Set(bucketKey int, qp *entity.QueryParams)
		Counter() uint64
	}

	Application struct {
		logger      *zap.Logger
		config      *config.Config
		pool        *xpg.Pool
		cpuUsage    CpuUsageRepository
		queryParams QueryParamsRepository
		reporter    *component.Reporter
	}
)

func New(logger *zap.Logger, cfg *config.Config, pool *xpg.Pool, cur CpuUsageRepository, qpr QueryParamsRepository, rep *component.Reporter) *Application {
	return &Application{
		logger:      logger,
		config:      cfg,
		pool:        pool,
		cpuUsage:    cur,
		queryParams: qpr,
		reporter:    rep,
	}
}

func (a *Application) Import(filename string) (er *dto.ExecResponse, err error) {
	// single Context with timeout for entire operation is fine for this case,
	// since it's easy to refactor to multiple contexts, if such need takes place
	ctx, cancel := context.WithTimeout(context.Background(), a.config.ImportTimeout())
	defer cancel()

	file, err := utils.OpenFileForRead(filename)
	if err != nil {
		return nil, err
	}
	defer func() { _ = file.Close() }()

	if er, err = a.cpuUsage.Import(ctx, file); err != nil {
		err = fmt.Errorf("import of csv file to 'cpu_usage' error: %w", err)
		return
	}

	return
}

func (a *Application) ExecuteQueries(filename string, quitCh chan<- os.Signal, stopCh <-chan struct{}, wg *sync.WaitGroup) (importErrors []string, err error) {
	file, err := utils.OpenFileForRead(filename)
	if err != nil {
		return nil, err
	}
	defer func() { _ = file.Close() }()

	importErrors, err = a.queryParams.Import(file)
	if err != nil {
		return
	}

	a.pool.SetMinConns(uint16(a.config.WorkersCount()))

	queryParamsChMap := make([]chan *entity.QueryParams, a.config.WorkersCount())
	idleChMap := make([]chan struct{}, a.config.WorkersCount())

	for i := 0; i < a.config.WorkersCount(); i++ {
		queryParamsChMap[i] = make(chan *entity.QueryParams)
		idleChMap[i] = make(chan struct{}, 1)
		go a.worker(i, queryParamsChMap[i], idleChMap[i], stopCh, wg)
	}

	go func() {
		runtime.Gosched()

		for {
			if a.queryParams.Counter() == 0 {
				quitCh <- syscall.SIGQUIT
				return
			}

			for i := 0; i < a.config.WorkersCount(); i++ {
				select {
				case <-stopCh:
					return
				case <-idleChMap[i]:
					qp := a.queryParams.Get(i)
					if qp == nil {
						continue
					}

					a.reporter.Add(qp)
					wg.Add(1)
					queryParamsChMap[i] <- qp
				default:
				}
			}
		}
	}()

	return
}

func (a *Application) Report(sig os.Signal, importErrors []string) dto.ReportResponse {
	rr := a.reporter.Report()
	rr.Success = len(importErrors) == 0 && sig == syscall.SIGQUIT
	rr.Signal = sig.String()
	rr.ImportErrors = importErrors

	return rr
}

func (a *Application) SetWorkersCount(i uint) error {
	a.queryParams.MakeBuckets(i)

	return a.config.SetWorkersCount(i)
}

func (a *Application) SetDebugMode() {
	a.config.SetDebugMode()
}

func (a *Application) worker(
	workerID int,
	queryParamsCh <-chan *entity.QueryParams,
	idleCh chan<- struct{},
	stopCh <-chan struct{},
	wg *sync.WaitGroup) {

	runtime.Gosched()

	idleCh <- struct{}{}

	for {
		select {
		case <-stopCh:
			return
		case qp := <-queryParamsCh:
			ctx, cancel := context.WithTimeout(context.Background(), a.config.ImportTimeout())
			er, err := a.cpuUsage.ExecuteQuery(ctx, qp)
			if err != nil {
				qp.Error = err.Error()
			} else {
				qp.Success = er.Success
			}
			cancel()

			qp.WorkerID = workerID
			qp.Duration = er.Duration

			if a.config.DebugMode() {
				utils.PrettyPrintJson(qp)
			}

			wg.Done()
			idleCh <- struct{}{}
		}
	}
}
