package config

import (
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"math"
	"os"
	"time"
)

const (
	PgConnStringEnvVar  = "CPUSAGE_PG_CONN_STR"
	DefaultWorkersCount = 1
)

type (
	// Config is storage struct used to hold configuration values.
	Config struct {
		pgConnStr    string
		workersCount int
		debugMode    bool
	}
)

func NewConfig() (*Config, error) {
	pgConnStr := os.Getenv(PgConnStringEnvVar)
	if pgConnStr == "" {
		return nil, fmt.Errorf("'%s' environment variable is not set", PgConnStringEnvVar)
	}

	return &Config{
		pgConnStr:    pgConnStr,
		workersCount: DefaultWorkersCount,
	}, nil
}

func (c *Config) PgConnStr() string {
	return c.pgConnStr
}

func (c *Config) MinPgConns() int {
	return 16
}

func (c *Config) MaxPgConns() int {
	return 128
}

func (c *Config) WorkersCount() int {
	return c.workersCount
}

func (c *Config) SetWorkersCount(i uint) error {
	if i == 0 {
		return fmt.Errorf("workers count cannot be '0'")
	}
	if i > math.MaxUint16 {
		return fmt.Errorf("workers count cannot greater then '%d'", math.MaxUint16)
	}

	c.workersCount = int(i)

	return nil
}

func (c *Config) ImportTimeout() time.Duration {
	return 30 * time.Second
}

func (c *Config) QueryTimeout() time.Duration {
	return 3 * time.Second
}

func (c *Config) TimePrecision() time.Duration {
	return time.Millisecond
}

func (c *Config) TimeLayout() string {
	return "2006-01-02 15:04:05"
}

func (c *Config) DebugMode() bool {
	return c.debugMode
}

func (c *Config) SetDebugMode() {
	c.debugMode = true
}
