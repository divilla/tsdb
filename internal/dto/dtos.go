package dto

import (
	"gitlab.com/divilla/tsdb/internal/entity"
	"time"
)

type (
	ReportResponse struct {
		Success      bool                  `json:"success"`
		Signal       string                `json:"signal"`
		Count        int                   `json:"query_count"`
		TotalTime    string                `json:"total_time"`
		MinTime      string                `json:"minimum_time"`
		MaxTime      string                `json:"maximum_time"`
		MedianTime   string                `json:"median_time"`
		AverageTime  string                `json:"average_time"`
		ImportErrors []string              `json:"import_errors"`
		ExecErrors   []*entity.QueryParams `json:"exec_errors"`
	}

	ExecResponse struct {
		Operation    string        `json:"operation"`
		Success      bool          `json:"success"`
		RowsAffected int64         `json:"rows_affected"`
		Time         string        `json:"time"`
		Duration     time.Duration `json:"-"`
	}
)

func NewExecResponse(op string) *ExecResponse {
	return &ExecResponse{
		Operation: op,
		Success:   true,
	}
}
