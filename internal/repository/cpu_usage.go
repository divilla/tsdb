package repository

import (
	"context"
	"fmt"
	"gitlab.com/divilla/tsdb/internal/dto"
	"gitlab.com/divilla/tsdb/internal/entity"
	"gitlab.com/divilla/tsdb/pkg/xpg"
	"go.uber.org/zap"
	"io"
	"time"
)

type (
	CpuUsage struct {
		logger *zap.Logger
		pool   *xpg.Pool
	}
)

func NewCpuUsage(logger *zap.Logger, pool *xpg.Pool) *CpuUsage {
	return &CpuUsage{
		logger: logger,
		pool:   pool,
	}
}

func (r *CpuUsage) Import(ctx context.Context, file io.Reader) (er *dto.ExecResponse, err error) {
	er = dto.NewExecResponse("import")

	start := time.Now()

	conn, err := r.pool.Acquire(ctx)
	if err != nil {
		err = fmt.Errorf("database connection failed: %w", err)
		return
	}
	defer conn.Release()

	sql := `truncate table cpu_usage;`
	_, err = conn.Exec(ctx, sql)
	if err != nil {
		r.logger.Fatal("truncate table command failed", zap.Error(err))
		return
	}

	sql = `copy cpu_usage from stdin delimiter ',' csv header;`
	res, err := conn.Conn().PgConn().CopyFrom(ctx, file, sql)
	if err != nil {
		err = fmt.Errorf("import of csv file failed with error: %w", err)
		return
	}

	er.Success = true
	er.RowsAffected = res.RowsAffected()
	er.Duration = time.Since(start)
	er.Time = er.Duration.String()
	return
}

func (r *CpuUsage) ExecuteQuery(ctx context.Context, qp *entity.QueryParams) (er *dto.ExecResponse, err error) {
	er = dto.NewExecResponse("report")

	start := time.Now()

	conn, err := r.pool.Acquire(ctx)
	if err != nil {
		return
	}
	defer conn.Release()

	sql := `
		select
			substr(ts::text, 1, 16) as minute,
			min(usage) as min_usage,
			max(usage) as max_usage
		from cpu_usage
		where
			host = $1
			and ts >= $2
			and ts <= $3
		group by
			substr(ts::text, 1, 16)
		order by minute desc;
	`

	res, err := conn.Exec(ctx, sql, qp.Hostname, qp.StartTime, qp.EndTime)
	if err != nil {
		return
	}

	er.Success = true
	er.RowsAffected = res.RowsAffected()
	er.Duration = time.Since(start)
	er.Time = er.Duration.String()
	return
}
