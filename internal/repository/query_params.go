package repository

import (
	"encoding/csv"
	"fmt"
	"gitlab.com/divilla/tsdb/internal/config"
	"gitlab.com/divilla/tsdb/internal/entity"
	"go.uber.org/zap"
	"hash/fnv"
	"io"
	"time"
)

type (
	// QueryParams is not concurrency-safe
	QueryParams struct {
		logger  *zap.Logger
		config  *config.Config
		buckets [][]*entity.QueryParams
		counter uint64
	}
)

func NewQueryParams(logger *zap.Logger, cfg *config.Config) *QueryParams {
	return &QueryParams{
		logger: logger,
		config: cfg,
	}
}

func (r *QueryParams) MakeBuckets(i uint) {
	r.buckets = make([][]*entity.QueryParams, i)
}

func (r *QueryParams) Get(bucketKey int) *entity.QueryParams {
	r.validateBucketKey(bucketKey)

	if len(r.buckets[bucketKey]) == 0 {
		return nil
	}

	qp := r.buckets[bucketKey][0]
	r.buckets[bucketKey] = r.buckets[bucketKey][1:]
	r.counter--

	return qp
}

func (r *QueryParams) Set(bucketKey int, qp *entity.QueryParams) {
	r.validateBucketKey(bucketKey)

	r.buckets[bucketKey] = append(r.buckets[bucketKey], qp)
	r.counter++
}

func (r *QueryParams) Import(rdr io.Reader) ([]string, error) {
	var importErrors []string

	var line int
	reader := csv.NewReader(rdr)
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			importErrors = append(importErrors, err.Error())
			continue
		}
		line++

		qp, err := r.parseCsvLine(line, record)
		if err != nil && line > 1 {
			importErrors = append(importErrors, err.Error())
			continue
		}
		if err != nil {
			continue
		}

		// while I'm fully aware that with this example hashing is probably not the best solution,
		// IMHO, it is any real world, larger scale scenarios, due low memory consumption & no need for extra maps
		bucketId, err := r.hashToBucketId(qp.Hostname)
		if err != nil {
			importErrors = append(importErrors, fmt.Sprintf("hashing '%s' on line %d error: %v", qp.Hostname, line, err))
		}

		r.Set(bucketId, qp)
	}

	return importErrors, nil
}

func (r *QueryParams) Counter() uint64 {
	return r.counter
}

func (r *QueryParams) validateBucketKey(bucketKey int) {
	if bucketKey < 0 || bucketKey >= len(r.buckets) {
		r.logger.Fatal("invalid bucket key", zap.Int("bucket_key", bucketKey))
	}
}

func (r *QueryParams) parseCsvLine(line int, record []string) (*entity.QueryParams, error) {
	var err error
	qp := new(entity.QueryParams)
	hostname := record[0]
	start := record[1]
	end := record[2]

	if len(hostname) == 0 {
		return nil, fmt.Errorf("'hostname' is missing on line %v", line)
	} else {
		qp.Hostname = hostname
	}

	if len(start) == 0 {
		return nil, fmt.Errorf("'start_time' is missing on line %v", line)
	} else if qp.StartTime, err = time.Parse(r.config.TimeLayout(), start); err != nil {
		return nil, fmt.Errorf("invalid 'start_time' on line %v: %w", line, err)
	}

	if len(end) == 0 {
		return nil, fmt.Errorf("'end_time' is missing on line %v", line)
	} else if qp.EndTime, err = time.Parse(r.config.TimeLayout(), end); err != nil {
		return nil, fmt.Errorf("invalid 'end_time' on line %v: %w", line, err)
	}

	if qp.StartTime.After(qp.EndTime) {
		return nil, fmt.Errorf("'start_time' must be before 'end_time' on line %v", line)
	}

	return qp, nil
}

func (r *QueryParams) hashToBucketId(s string) (res int, err error) {
	h := fnv.New32a()
	if _, err = h.Write([]byte(s)); err != nil {
		return
	}

	res = int(h.Sum32()) % r.config.WorkersCount()
	return
}
