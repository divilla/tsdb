package entity

import (
	"time"
)

type (
	QueryParams struct {
		Hostname  string        `json:"hostname"`
		StartTime time.Time     `json:"start_time"`
		EndTime   time.Time     `json:"end_time"`
		Success   bool          `json:"success"`
		WorkerID  int           `json:"worker_id"`
		Duration  time.Duration `json:"execution_time"`
		Error     string        `json:"errors"`
	}
)
