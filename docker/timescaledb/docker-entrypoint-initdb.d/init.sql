CREATE EXTENSION IF NOT EXISTS timescaledb;

CREATE TABLE cpu_usage(
    ts      TIMESTAMPTZ,
    host    TEXT,
    usage   DOUBLE PRECISION
);

SELECT create_hypertable('cpu_usage', 'ts');

CREATE USER cpuser WITH ENCRYPTED PASSWORD 'pass';
GRANT ALL ON DATABASE cpusage TO cpuser;
ALTER SCHEMA public OWNER TO cpuser;
ALTER TABLE cpu_usage OWNER TO cpuser;
