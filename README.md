# cpusage

## Getting started

Although it was not necessary with this particular solution, I have habit of using Bitnami containers, because they run rootless out of the box, always up to date & I've never had any security, performance or configurability issues with them. Bitnami also provides Helm charts.

```shell
# This is building directory structure for Bitnami container volumes
sudo ./docker-prepare.sh

# List make commands
make
```

## Docker compose

As it was specified in assignment, it does everything, from downloading & preparing containers, building db structure, importing data, stage building application, executing with 64 workers & printing report.

```shell
# Simply run with
docker compose up

# After Ctrl+C, remove containers
docker compose down
```

## Manual run

```shell
# Run timescaledb docker on localhost
make db-up

# Build app
make build

# Show help
./cpusage

# Import data
./cpusage import data/cpu_usage.csv

# Execute queries & build report
./cpusage exec --workers 8 data/query_params.csv
./cpusage exec -w 8 data/query_params.csv

# Execute queries & build report in debug mode
./cpusage exec --workers 8 --debug data/query_params.csv
./cpusage exec -w 8 -d data/query_params.csv

# Stop timescaledb docker
make db-down
```

## Decision points

* I've used environment variable for DB connection, as I don't really know better way to do it. If it were cloud and/or k8s solution, I would use secrets or vault.
* I used hashing as method to distribute jobs over workers, while fulfilling rule: 'same host should be processed by the same worker', because it's fastest, shortest & IMHO most common strategy in similar cases.
* 'Application.ExecuteQueries()' could have been done using mutexes and/or assigning jobs in separate set of workers. This approach simplifies the code without any performance penalties. Please feel free to test for Race Conditions.
* I used QueryParams repository to store query params in memory, so jobs can be assigned to 'buckets', then pulled from buckets & channeled to workers. QueryParams repository can be easily re-factored to use DB as persistence layer & handle unlimited data size. I couldn't figure out approach where 'query_params.csv' can be red dynamically without doubling number of goroutines (worker helpers). Third option was to use buffered channels, but I dismissed that approach, as it would break on large data set.
* I've built separate Reporter component for the sake of separation of concerns, re-usability, readability & testability.
* While 'caching' queryParams in memory is obvious flaw in case of Big Data scenario - I would still implement it similar, reading line by line & writing readings (times) back to database. Reporter would then query db to make final calculations. While that would certainly be less performant solution & require additional disk space, in the long run it would simplify upgrades, maintenance & code readability.

## Authors and acknowledgment
Vito Di Villa is my pseudonym, so 'divilla' is namespace.

## Project status
* I have had the best intentions to write tests, however, I failed to do so due to need to take care of some difficult unexpected events. However, if writing tests will improve my rating, I'm willing to do it, but please don't expect full dedication, it might take few days.
* The app is built using dependency injection, having all dependencies injected in main.go. I'm aware that is nonsense in console app, but didn't have time to fix it. If you require, I'll rearrange the app to load dependencies 'when needed'